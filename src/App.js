import './App.css';
import Test from './pages/Test';
// import Test2 from './pages/Test2';
import TodoProvider from './store/todo';

function App() {
  return (
    <TodoProvider>
      <div className="App">
        <Test />
        {/* <Test2 /> */}
      </div>
    </TodoProvider>
  );
}


export default App;
