import {useReducer} from "react"

const useActionReducer = ({ reducer={}, initialState={} }, actions={}) =>{
    const [state, dispatch] = useReducer(reducer, initialState)

    let actionCreator = {}
    actions && typeof actions === 'object' && Object.keys(actions).forEach(key => {
        actionCreator[key] = (...args) => {
            return actions[key](...args)({ dispatch, state })
        }
    })

    return {state, actions: actionCreator}
}


export default useActionReducer