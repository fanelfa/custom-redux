import React from 'react'

const connectStore = (useStore, useDispatch) => {
    return (statesToProps = (state)=>{}, actionsToProps = {}) =>{
        return (WrappedComponent) => {
            return (props) => {
                const dispatchFromStore = useDispatch()
                const stateFromStore = useStore()

                let actionsProps = {}
                Object.keys(actionsToProps).forEach(key => {
                    actionsProps[key] = (...args)=>{
                        return actionsToProps[key](...args)({
                            dispatch: dispatchFromStore,
                            state : stateFromStore,
                        })
                    }
                })

                const statesProps = statesToProps(stateFromStore)
                
                return (
                    <WrappedComponent {...props} {...statesProps} {...actionsProps} />
                )
            }
        }
    }
}

export default connectStore