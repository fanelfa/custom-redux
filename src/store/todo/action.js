import { ADD_TODO } from "./types"

export const addTodo = (todo) => ({ dispatch }) => {
    dispatch({type: ADD_TODO, payload:{todo}});
    // setTimeout(() => {
    //     dispatch({ type: ADD_TODO, payload: { todo } });
    // }, 3000);
    // clearTimeout(timer);
}

export const showTodo = () => ({ state }) => {
    console.log(state);
    // console.log(id);
}