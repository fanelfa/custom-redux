import { FETCH_TODO, ADD_TODO, UPDATE_TODO, DELETE_TODO } from "./types"

export const initialState = {
    successMessage: '',
    errorMessage:'',
    list:[],
    item:null
}

const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_TODO: return state;
        case ADD_TODO: return {
            ...state, 
            list: [...state.list, action.payload.todo]
        };
        case UPDATE_TODO: return {
            ...state,
            list: [
                state.list.filter(item => item.id !== action.payload.id),
                action.payload.todo
            ]
        };
        case DELETE_TODO: return {
            ...state,
            list: state.list.filter(item => item.id !== action.payload.id)
        };
        default: return state;
    }
}


export default reducer;

export const todoReducerWithInit = {reducer, initialState}