import makeStore from '../config/makeStore'
import connectStore from '../config/connectStore'

import reducer, { initialState } from './reducer'

const [
    TodoProvider,
    useTodoStore,
    useTodoDispatch
] = makeStore(reducer, initialState)

export default TodoProvider

export const connectTodoStore = connectStore(useTodoStore, useTodoDispatch)