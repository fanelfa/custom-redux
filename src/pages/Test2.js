import React, { useEffect } from 'react'
import useActionReducer from '../store/config/useActionReducer';
import { addTodo, showTodo } from '../store/todo/action';
import { todoReducerWithInit } from '../store/todo/reducer';


const Test2 = props => {
    const {state, actions} = useActionReducer(todoReducerWithInit, { addTodo, showTodo });

    useEffect(() => {
        // actions.addTodo('minum');

        // eslint-disable-next-line
    }, [])
    return (
        <>
            <h1>Test 2 {state.list}</h1>
            <button onClick={() => actions.addTodo('makan')}>console todo</button>
        </>
    )
}

export default Test2;