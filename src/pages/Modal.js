import React, { memo, useEffect } from 'react'
import { connectTodoStore } from '../store/todo'


const Modal = props => {
    useEffect(()=>{
        console.log('render')
    },[])
    return (
        <div
            style={{
                position:'fixed',
                bottom:'0',
                right:'0',
                width: '30%'
            }}
        >
            <h2>Modal Todo</h2>
            {
                props.list.map((item, index) => {
                    return <h5 key={item + '' + index}>{item}</h5>
                })
            }
        </div>
    )
}

const mstp = state => ({
    list : state.list
});

const areEqual = (prevProps, nextProps) =>{
    return prevProps.list === nextProps.list
}

export default memo(connectTodoStore(mstp)(Modal), areEqual);