import React, { useEffect, useState } from 'react'
import { connectTodoStore } from '../store/todo'
import { addTodo, showTodo } from '../store/todo/action';
import Modal from './Modal';


const Test = props => {
    const initLocalState = {
        todo: ''
    }
    const [localState, setLocalState] = useState(initLocalState);

    const onSubmit = e =>{
        e.preventDefault();
        localState.todo !== '' && props.addTodo(localState.todo);
        setLocalState(initLocalState);
    }

    const onChangeHandler = e => {
        const value = e.target.value;
        const name = e.target.name;
        setLocalState({
            [name]: value
        });
    }

    useEffect(()=>{
        console.log('render')
    },[])
    
    return (
        <>
            <h1>Test</h1>
            <button onClick={()=>props.showTodo()}>console todo</button>
            <br/>
            <br/>
            <form onSubmit={e => onSubmit(e)}>
                <input 
                    name="todo" type="text" required
                    onChange={(e)=>onChangeHandler(e)}
                    value={localState.todo}
                ></input>
                <button type="submit">Add</button>
            </form>
            {
                props.list.map((item, index)=>{
                    return <h5 key={item+''+index}>{item}</h5>
                })
            }
            <Modal/>
        </>
    )
}

const actionsToProps = {
    addTodo,
    showTodo
}

const stateToProps = state =>({
    list: state.list
})


export default connectTodoStore(stateToProps, actionsToProps)(Test);